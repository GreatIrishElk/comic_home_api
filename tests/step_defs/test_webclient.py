from pytest_bdd import scenarios, given, when, then, parsers
import pytest
from requests_html import HTMLSession
from http import HTTPStatus
from pathlib import Path
from table_parser.table_parser import table_parser
from scrapers.marvel_database_scraper import MarvelDatabaseScraper
import re
import json


EXTRA_TYPES = {
    'Number': int,
    'String': str,
}
CONVERTERS = {
    'title': str,
    'vol': int,
    'issue': int,
    'date': str,
    'comics': str
}
PORT = 8080
URL = 'http://localhost:8080/'


class Comic:
    def __init__(self):
        self.title = None
        self.vol = None
        self.issue = None


scenarios(
    'webclient.feature',
    example_converters=CONVERTERS
)


@pytest.fixture(scope='session')
def comic():
    yield Comic()


@pytest.fixture(scope='session')
def session():
    yield HTMLSession()


@given(parsers.cfparse('I want to send a request for "{title:String}"', extra_types=EXTRA_TYPES))
@given('I want to send a request for "<title>"')
def set_comic_title(comic, title):
    comic.title = title


@given(parsers.cfparse('the volume is "{vol:String}"', extra_types=EXTRA_TYPES))
@given('the volume is "<vol>"')
def set_comic_vol(comic, vol):
    comic.vol = vol


@given(parsers.cfparse('the issue is "{issue:String}"', extra_types=EXTRA_TYPES))
@given('the issue is "<issue>"')
def set_comic_issue(comic, issue):
    comic.issue = issue


@when('I send the request the comic date')
def request_comic_date(context, session, comic):
    context.response = session.get(
        f'{URL}comic-date?title={comic.title}&vol={comic.vol}&issue={comic.issue}'
    )
    context.comic = json.loads(context.response.text)


@then(parsers.cfparse('I will receive the response with the date "{date:String}"', extra_types=EXTRA_TYPES))
@then('I will receive the response with the date "<date>"')
def verify_date_from_request(context, date):
    assert context.comic['date'] == date


@when("I send the request the comic")
def step_impl(context, session, comic):
    context.response = session.get(f'{URL}comic?title={comic.title}&vol={comic.vol}&issue={comic.issue}')
    context.comic = json.loads(context.response.text)


@then("I will receive a OK response")
def verify_OK_response(context):
    assert context.response.status_code == HTTPStatus.OK.value


@then(parsers.cfparse('I will receive the response with the cover artists "{cover_artists}"'))
def verify_cover_artists_from_request(context, cover_artists):
    for artist in re.split(r' and ', cover_artists):
        assert artist in context.comic['cover artists']


@then(parsers.cfparse('I will receive the response with the cover artists:\n{cover_artists}'))
def verify_cover_artists_from_request(context, cover_artists):
    table = table_parser(cover_artists)
    for artist in table['Cover Artists']:
        artist = re.sub(r'\|', '', artist)
        artist = artist.strip()
        assert artist in context.comic['cover artists']


@then(parsers.cfparse('I will receive the response with the writer "{writers}"'))
def verify_writers_from_request(context, writers):
    for writer in re.split(r' and ', writers):
        assert writer in context.comic['writers']


@given(parsers.cfparse('I want to set the background to "{title}", vol "{vol}", issue "{issue}"'))
def step_impl(comic, title, vol, issue):
    comic.title = title
    comic.vol = vol
    comic.issue = issue


@when("I send the request to set the background")
def step_impl(context, session, comic):
    context.response = session.get(f'{URL}set-background?title={comic.title}&vol={comic.vol}&issue={comic.issue}')
    print(context.response)


@given('the date is "<date>"')
def step_impl(date):
    raise NotImplementedError(u'STEP: Given the date is "<date>"')


@when("I request comics published today")
def step_impl():
    raise NotImplementedError(u'STEP: When I request comics published today')


@then('I receive a response with the comics "<comics>"')
def step_impl(comics):
    raise NotImplementedError(u'STEP: Then I receive a response with the comics "<comics>"')


@when("I request the main cover")
def step_impl(context, session, comic):
    context.response = session.get(f'{URL}cover?title={comic.title}&vol={comic.vol}&issue={comic.issue}')
    print(context.response)


@when("I request the variant covers")
def step_impl(context, session, comic):
    context.response = session.get(f'{URL}cover-variant?title={comic.title}&vol={comic.vol}&issue={comic.issue}')


@then("I will verify that the main comic cover has been downloaded")
def step_impl(marvel_database_scraper, comic):
    response = marvel_database_scraper.fetch_issue(title=comic.title, volume=comic.vol, issue=comic.issue)
    artists = marvel_database_scraper.get_cover_artists(response)
    assert Path(
        re.sub(
            ' ',
            '.',
            f'../covers/{comic.title}/Vol {comic.vol}/{comic.title} Vol {comic.vol} {comic.issue} by {artists}.png')
    ).exists()


@then("I will verify that the variant comic covers have been downloaded")
def step_impl(marvel_database_scraper, comic):
    save_title = None
    response = marvel_database_scraper.fetch_issue(title=comic.title, volume=comic.vol, issue=comic.issue)
    divs = marvel_database_scraper.get_info_table_tables(response)[0].find('div')
    for div in divs:
        variant_title = marvel_database_scraper.get_variant_title(response, div.text)
        save_title = f'{comic.title} Vol {comic.vol} {comic.issue} {variant_title}.png'
    assert Path(re.sub(' ', '.', f'../covers/{comic.title}/Vol {comic.vol}/variants/{save_title}')).exists()
