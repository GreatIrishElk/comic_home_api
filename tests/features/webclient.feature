# Created by murph at 11/12/2020
Feature: Basic Responses
  Ability to respond to basic queries

  @webclient @cucumber
  Scenario Outline: Receive the date a comic was released
    Given I want to send a request for "<title>"
    And the volume is "<vol>"
    And the issue is "<issue>"
    When I send the request the comic date
    Then I will receive the response with the date "<date>"

    Examples:
      | title               | vol | issue | date        |
      | Avenging Spider-Man | 1   | 1     | 09-11-2011  |
      | Black Bolt          | 1   | 1     | 03-05-2017  |

  @webclient @cucumber
  Scenario: Request details about a specific comic
    Given I want to send a request for "Avenging Spider-Man"
    And the volume is "1"
    And the issue is "1"
    When I send the request the comic
    Then I will receive the response with the date "09-11-2011"
    And I will receive the response with the writer "Zeb Wells"
    And I will receive the response with the cover artists:
      | Cover Artists     |
      | Joe Madureira     |
      | Aron Lusen        |
      | Humberto Ramos    |
      | Edgar Delgado     |
      | J. Scott Campbell |
      | Joe Quesada       |
      | Danny Miki        |
      | Richard Isanove   |

  @webclient @cucumber
  Scenario: Send request to set background
    Given I want to set the background to "Avenging Spider-Man", vol "1", issue "1"
    When I send the request to set the background
    Then I will receive a OK response

  @webclient @cucumber
  Scenario Outline: Send request for comics published this day in history
    Given the date is "<date>"
    When I request comics published today
    Then I receive a response with the comics "<comics>"

    Examples:
      | date        | comics  |
      | 20/12/2020  | stuff   |
      | 12/01/2020  | stuff   |
      | 18/06/2020  | stuff   |

  @webclient @cucumber
  Scenario: Download covers
    Given I want to send a request for "Avenging Spider-Man"
    And the volume is "1"
    And the issue is "1"
    When I request the main cover
    Then I will verify that the main comic cover has been downloaded

  @webclient @cucumber
  Scenario: Download variant covers
    Given I want to send a request for "Avenging Spider-Man"
    And the volume is "1"
    And the issue is "1"
    When I request the variant covers
    Then I will verify that the variant comic covers have been downloaded

