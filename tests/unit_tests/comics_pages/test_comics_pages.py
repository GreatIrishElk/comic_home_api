from comic_pages.page import Page
from comic_pages.issue import MarvelDatabaseIssue
import pytest


@pytest.mark.unittests
class TestPage:

    def test_init_page(self):
        expected_answer = 'hello world'
        page = Page()
        assert page.page is None
        page = Page(page=expected_answer)
        assert page.page == expected_answer


@pytest.mark.unittests
class TestMarvelDatabaseVolume:

    def test_get_issues(self, marvel_db_vol_page):
        expected_num_issues = 19
        assert len(marvel_db_vol_page.issues) == expected_num_issues

    def test_get_issue(self, marvel_db_vol_page):
        issue = marvel_db_vol_page.get_issue(4)
        assert type(issue) == MarvelDatabaseIssue
        assert issue.publish_date == '07-2014'
        assert issue.release_date == '28-05-2014'

    def test_year_published(self, marvel_db_vol_page):
        issue = marvel_db_vol_page.get_issue(4)
        assert issue.year_published == '2014'

