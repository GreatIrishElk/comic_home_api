from scrapers.scraper import Scraper
from datetime import datetime
import requests
import re


div_class = '.az-list'
MARVEL = 'https://www.marvel.com'
EXTENSION = 'comics/series/{seriesID}/{seriesName}'
TOO_MANY = '?byZone=marvel_site_zone&offset=0&byType=comic_series&dateStart=&dateEnd=&orderBy=release_date+desc&byId=????&limit=54&count=20&totalcount=1000'


class MarvelWebsiteScraper(Scraper):
    SERIES = '/comics/series/'
    COMICS = '/comics/issue/'

    COVER_DIV = '.JCMultiRow-comic_issue'
    ISSUE_COVER = '.row-item-image-url'
    DATA_SECTION = '.featured-item-meta'

    def _request(self, base_url=MARVEL, extension: str = '') -> requests.Response:
        return super(MarvelWebsiteScraper, self)._request(
            base_url=base_url,
            extension=extension
        )

    @staticmethod
    def convert_link_to_extension(link):
        return re.split(MARVEL, link)[-1]

    @staticmethod
    def get_issue_number(title):
        return int(re.split(r'#', title)[-1])

    def get_series_divs(self):
        response = self._request(
            extension=f'{self.SERIES}'
        )
        return response.html.find(div_class)

    def get_details_in_div(self, div):
        details = []
        for link in div.find('a'):
            details.append(
                (
                    link.text,
                    self.convert_link_to_extension(self.get_absolute_links(link)[0])
                )
            )

        return details

    def get_titles(self):
        titles = []
        for div in self.get_series_divs():
            for marvel_title in self.get_details_in_div(div):
                if marvel_title:
                    if 'A Year of Marvels' in marvel_title[0]:
                        if 'A Year of Marvels' not in titles:
                            titles.append('A Year of Marvels')
                    else:
                        titles.append(re.split(r' \(', marvel_title[0])[0])
        return titles

    def get_volume_query(self, url):
        byid = re.split(self.SERIES, url)[-1]
        byid = re.split(r'\/', byid)[0]
        too_many = re.sub(r'\?\?\?\?', byid, TOO_MANY)
        return too_many

    def get_details_by_first_letter(self, title):
        details = None
        for div in self.get_series_divs():
            details = self.get_details_in_div(div)
            if details:
                if re.search('.', details[0][0])[0] == re.search('.', title)[0]:
                    break
        return details

    def get_title_urls(self, title, year):
        urls = []
        details_all = self.get_details_by_first_letter(title)
        for details in details_all:
            if re.search(f'^{title}', details[0]) and year in details[0]:
                urls.append(details[1])
                break
        return urls

    def get_issue_data(self, issue: requests.Response):
        if issue is not None:
            try:
                data = issue.html.find(self.DATA_SECTION)

                release_date = re.split(r'\n', data[0].text)[1]
                release_date = self.format_dates(release_date, in_format='%B %d, %Y', out_format='%d-%m-%Y')
                issue_data = {
                    'release date': release_date
                }
                return issue_data
            except ValueError:
                return None
        return None

    # @staticmethod
    # def get_volume_query(comic_id):
    #     return re.sub(r'\?\?\?\?', str(comic_id), TOO_MANY)

    def get_volume_pages(self, title, year):
        urls = []
        responses = []
        if re.search(r'^(The |)Amazing Spider-Man$', title):
            titles = ['The Amazing Spider-Man', 'Amazing Spider-Man']
            years = ['1963', '1999']
        else:
            titles = [title]
            years = [year]
        for title in titles:
            for year in years:
                urls = self.get_title_urls(title, year)
        for url in urls:
            if url:
                response = self._request(extension=f'{url}{self.get_volume_query(url)}')
                if self.is_response_ok(response):
                    responses.append(response)
        return responses

    def get_issues(self, volume: requests.Response, use_query=True):
        if not use_query:
            extension = re.split(r'\?', volume.url)[0]
            extension = re.split(MARVEL, extension)[-1]
            volume = self._request(extension=extension)
        covers = volume.html.find(self.COVER_DIV)
        if covers:
            images = covers[0].find(self.ISSUE_COVER)
            images.reverse()
            if not images:
                images = self.get_issues(volume, use_query=False)
            return images
        else:
            return volume

    def get_issue(self, images, issue_number) -> requests.Response:
        try:
            asb_links = self.get_absolute_links(images[int(issue_number) - 1])
            link = self.convert_link_to_extension(asb_links[0])
            return self._request(extension=link)
        except IndexError:
            return None
    #
    # def get_issue_data(self, issue: requests.Response):
    #     data = issue.html.find(self.DATA_SECTION)
    #
    #     release_date = re.split(r'\n', data[0].text)[1]
    #     release_date = datetime.strptime(release_date, '%B %d, %Y')
    #     release_date = release_date.strftime('%d-%m-%Y')
    #     issue_data = {
    #         'release date': release_date
    #     }
    #     return issue_data

    def get_release_date(self, title, year, issue_number):
        volume_pages = self.get_volume_pages(title, year)
        if volume_pages:
            for vol in volume_pages:
                issues = self.get_issues(vol)
                if type(issues) == list:
                    issue = self.get_issue(issues, issue_number)
                else:
                    issue = issues
                data = self.get_issue_data(issue)
                if data is not None:
                    return data['release date']
                else:
                    return 'n/a'
        else:
            return 'n/a'


if __name__ == '__main__':
    scraper = MarvelWebsiteScraper()
    print(scraper.get_release_date('Astonishing X-Men', '2004', '6'))
    # for r in scraper.get_titles():
    #     print(repr(r))
