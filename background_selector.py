import time

from requests_html import HTMLSession
from comic_pages.issue import MarvelDatabaseIssue
import csv
import re
from datetime import datetime, timedelta
from argparse import ArgumentParser
from pathlib import Path
import os
import random


class BackgroundSelector:
    SET_BACKGROUND = 'gsettings set org.gnome.desktop.background picture-uri'

    def __init__(self, **kwargs):
        self.session = HTMLSession()
        self.day = datetime.now()
        self.todays_issues = []
        self.used_today = []
        if 'file_name' in kwargs:
            f = open(kwargs['file_name'], 'r', encoding="utf-8")
            self.csv_reader = csv.DictReader(f, delimiter=',', quotechar='"')
        self.time_last_changed = self.day

    def set_background(self, issue: MarvelDatabaseIssue):
        path_to_background = Path(__file__).parent.absolute()
        path_to_background = f'{path_to_background}/covers'
        path_to_background = f'{path_to_background}/{issue.title}/Vol {issue.volume}'
        image_name = f'{issue.title} Vol {issue.volume} Issue {issue.issue_number} by {issue.cover_artists}.png'
        path_to_background = f'{path_to_background}/{image_name}'
        path_to_background = re.sub(r' ', '.', path_to_background)
        path_to_background = re.sub(r":", ";", path_to_background)
        path_to_background = re.sub(r"n/a|N/A", "na", path_to_background)
        print(f'Setting background to "{path_to_background}"')
        self.time_last_changed = datetime.now()
        if os.name == 'posix':
            os.system(f'{self.SET_BACKGROUND} "file://{path_to_background}"')
        else:
            print(f'Failed to set background. OS is: {os.name}')

    def interval_elapsed(self):
        interval_given = self.get_interval()
        now = datetime.now()
        elapsed = now - self.time_last_changed
        return elapsed >= timedelta(minutes=interval_given)

    def get_day_month(self, date):
        if type(date) != datetime:
            d = datetime.strptime(date, '%d-%m-%Y')
        else:
            d = date
        return d.strftime('%d-%m')

    def get_todays_issues(self):
        self.todays_issues = []
        for row in self.csv_reader:
            if row['Release Date'].upper() != 'N/A':
                release_day_month = self.get_day_month(row['Release Date'])
                if release_day_month == self.day.strftime('%d-%m'):
                    self.todays_issues.append(
                        MarvelDatabaseIssue(
                            title=row['Title'],
                            volume=row['Volume'],
                            issue_number=row['Issue Number']
                        ))

    def select_background(self):
        choice = random.choice(self.todays_issues)
        if self.used_today == self.todays_issues:
            return choice
        elif choice not in self.used_today:
            self.used_today.append(choice)
            return choice
        else:
            return self.select_background()

    def download_background(self, issue: MarvelDatabaseIssue):
        issue.get_main_cover(title=f'{issue.title} Vol {issue.volume} Issue {issue.issue_number}')

    def new_day(self):
        current = self.get_day_month(datetime.now())
        last_know = self.get_day_month(self.day)
        if current != last_know:
            self.day = datetime.now()
        return current != last_know

    def get_interval(self):
        return 720 / len(self.todays_issues)





def parse_arguments():
    parser = ArgumentParser(
        description='Application to download Marvel Comic covers and information about them.'
    )
    parser.add_argument(
        '-f', dest='file_path', action='store',
        help='Path to yaml file with comic_pages titles and volumes to download.'
    )
    return parser.parse_args()


def download_covers(session, row):
    session.get(
        f'http://localhost:8080/cover?title={row["Title"]}&vol={row["Volume"]}&issue={row["Issue Number"]}')
    session.get(
        f'http://localhost:8080/cover-variant?title={row["Title"]}&vol={row["Volume"]}&issue={row["Issue Number"]}')

    print(f'{row["Title"]} {row["Volume"]} {row["Issue Number"]} - {row["Release Date"]}')
    return Path(
        re.sub(
            ' ',
            '.',
            f'covers/{row["Title"]}/Vol {row["Volume"]}/{row["Title"]} Vol {row["Volume"]} {row["Issue Number"]} by {row["Cover Artists"]}.png'
        )
    ).absolute()


def new_day(day):
    if not day:
        return True
    else:
        current = datetime.now()
        current = current.strftime('%d-%m')
        day = day.strftime('%d-%m')
        return current != day


def set_background(absolute_paths):
    choice = random.choice(absolute_paths)
    absolute_paths.pop(absolute_paths.index(choice))
    print(choice)
    os.system(f'gsettings set org.gnome.desktop.background picture-uri "file://{re.sub(r":", ";", str(choice))}"')
    return absolute_paths


def interval_elapsed(day, interval_given):
    now = datetime.now()
    elapsed = now - day
    return elapsed >= timedelta(minutes=interval_given)


def check_command():
    if Path('.command').exists():
        try:
            f = open('.command', 'r')
            com = f.read()
            f.close()
            Path('.command').unlink()
        except IOError:
            return check_command()
        return com
    else:
        return None


if __name__ == '__main__':
    options = parse_arguments()
    selector = BackgroundSelector(file_name=options.file_path)
    selector.get_todays_issues()
    command = None
    run = True

    while run:
        if not selector.used_today or selector.interval_elapsed() or command == '/next':
            c = selector.select_background()
            selector.download_background(c)
            selector.set_background(c)
        elif command == '/stop':
            print('Stopping now.')
            run = False
        elif selector.new_day():
            selector.get_todays_issues()
        time.sleep(60)
        command = check_command()


    # session = HTMLSession()
    # options = parse_arguments()
    # today = None
    # paths = []
    # interval = None
    # if options.file_path:
    #     while True:
    #         if new_day(today):
    #             paths = []
    #             today = datetime.now()
    #             with open(options.file_path, 'r', encoding="utf-8") as file:
    #                 csv_reader = csv.DictReader(file, delimiter=',', quotechar='"')
    #                 for row in csv_reader:
    #                     if row["Release Date"] != 'n/a':
    #                         release_day_month = f'{re.split("-", row["Release Date"])[0]}-{re.split("-", row["Release Date"])[1]}'
    #                         if release_day_month == today.strftime('%d-%m'):
    #                             paths.append(download_covers(session, row))
    #             paths = set_background(paths)
    #             interval = 1440 / len(paths)
    #             print(interval)
    #         elif interval_elapsed(today, interval):
    #             today = datetime.now()
    #             paths = set_background(paths)
    #         time.sleep(60)
