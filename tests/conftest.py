import pytest
from pytest_bdd import scenario, given, when, then, parsers
from comic_pages.volume import MarvelDatabaseVolume
from comic_pages.issue import MarvelDatabaseIssue
from scrapers.marvel_database_scraper import MarvelDatabaseScraper
from webclient.comics_api import ComicsAPI


PORT = 8080


def pytest_bdd_step_error(request, feature, scenario, step, step_func, step_func_args, exception):
    print(f'Step failed: {step}')


@pytest.fixture(autouse=True)
def context():
    class Context(object):
        pass

    return Context()


@pytest.fixture(scope='function')
def marvel_db_vol_page():
    yield MarvelDatabaseVolume(title='Ms. Marvel', vol='3')


@pytest.fixture(scope='function')
def marvel_db_issue_page():
    yield MarvelDatabaseIssue(title='Ms. Marvel', volume='3', issue_number='1')


@pytest.fixture(scope='function')
def marvel_database_scraper():
    yield MarvelDatabaseScraper()


@pytest.fixture(scope='function')
def comics_api():
    yield ComicsAPI()

