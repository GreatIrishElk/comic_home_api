from comic_pages.page import Page
from comic_pages.issue import MarvelDatabaseIssue
from requests_html import HTMLSession
from scrapers.marvel_database_scraper import MarvelDatabaseScraper


class MarvelDatabaseVolume(Page):
    MARVEL_DATABASE = "https://marvel.fandom.com/"

    def __init__(self, **kwargs):
        super(MarvelDatabaseVolume, self).__init__(**kwargs)
        if 'title' in kwargs:
            self._title = kwargs['title']
        else:
            self._title = None
        if 'vol' in kwargs:
            self._vol = kwargs['vol']
        else:
            self._vol = None
        if self.title and self.vol:
            self._issues = self.get_issues()
        elif 'issues' in kwargs:
            self._issues = kwargs['issues']
        else:
            self._issues = []

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title

    @property
    def vol(self):
        return self._vol

    @vol.setter
    def vol(self, vol):
        self._vol = int(vol)

    @property
    def issues(self):
        return self._issues

    @issues.setter
    def issues(self, issues):
        self._issues = issues

    def get_issue(self, issue_number):
        session = HTMLSession()
        return MarvelDatabaseIssue(
            page=session.get(f'{self.MARVEL_DATABASE}{self.issues[issue_number - 1]}'),
            title=self.title,
            volume=self.vol,
            issue_number=issue_number
        )

    def get_issues(self):
        return MarvelDatabaseScraper().get_issues(title=self.title, vol=self.vol)

