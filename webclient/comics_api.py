import re
import enum
from scrapers.marvel_database_scraper import MarvelDatabaseScraper


DATA = {
    'Black Bolt': {
        'volumes': {
            '1': {
                '1': {
                    'cover artists': [
                        'Christian Ward'
                    ],
                    'writers': [
                        'Saladin Ahmed'
                    ],
                    'date': '03-05-2017'
                }
            }
        }
    },
    'Avenging Spider-Man': {
        'volumes': {
            '1': {
                '1': {
                    'cover artists': [
                        'Joe Madureira',
                        'Aron Lusen',
                        'Humberto Ramos',
                        'Edgar Delgado',
                        'J. Scott Campbell',
                        'Joe Quesada',
                        'Danny Miki',
                        'Richard Isanove'

                    ],
                    'writers': [
                        'Zeb Wells'
                    ],
                    'date': '09-11-2011'
                }
            }
        }
    }
}


class APICommands(enum.Enum):
    GET_DATE = '/comic-date'
    GET_COMIC = '/comic'
    SET_BACKGROUND = '/set-background'
    REQUEST_COVER = '/cover'
    REQUEST_COVER_VARIANT = '/cover-variant'
    NEXT_BACKGROUND = "/next"
    STOP = "/stop"


class ComicsAPI:
    COMMAND_FILE = '.command'

    def __init__(self):
        self.scraper = MarvelDatabaseScraper()

    def sanitize(self, text):
        text = re.sub(r'%20', ' ', text)
        return text

    def get_options(self, url: str):
        options = {}
        if re.search(r'\?', url):
            command, args = re.split(r'\?', url)
            args = re.split(r'&', args)
            for arg in args:
                option, value = re.split(r'=', arg)
                options[option] = value
        else:
            command = url
            options = None
        return command, options

    def get_details(self, title, vol, issue):
        title = self.sanitize(title)
        return DATA[title]['volumes'][vol][issue]

    def get_date(self, title, vol, issue):
        return {
            'date': self.get_details(title, vol, issue)['date']
        }

    def request_cover(self, title, vol, issue):
        title = re.sub(r'%20', ' ', title)
        for_file_name = f'{title} Vol {vol} {issue}'
        request = self.scraper.fetch_issue(title=title, volume=vol, issue=issue)
        self.scraper.get_main_cover(
            request,
            title=for_file_name,
            image_extension='png',
            directory=re.sub(' ', '.', f'covers/{title}/Vol {vol}')
        )

    def request_variant_cover(self, title, vol, issue):
        title = re.sub(r'%20', ' ', title)
        t = f'{title} Vol {vol} {issue}'
        request = self.scraper.fetch_issue(title=title, volume=vol, issue=issue)
        self.scraper.get_variant_covers(
            request,
            title=t,
            image_extension='png',
            directory=re.sub(' ', '.', f'covers/{title}/Vol {vol}/variants')
        )

    def write_command(self, command):
        f = open(self.COMMAND_FILE, 'w')
        f.write(command)
        f.close()
