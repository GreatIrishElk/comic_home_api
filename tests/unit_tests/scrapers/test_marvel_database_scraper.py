from scrapers.marvel_database_scraper import MarvelDatabaseScraper


class TestMarvelDataBase:

    def test_get_details(self, marvel_db_issue_page):
        expected_colourist = 'Ian Herring'
        scraper = MarvelDatabaseScraper()
        actual_colourist = scraper.get_colourist(marvel_db_issue_page.page)
        assert actual_colourist == expected_colourist, f'Expected: {expected_colourist}' \
                                                       f'Actual: {actual_colourist}'
