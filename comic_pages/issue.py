from comic_pages.page import Page
from scrapers.marvel_database_scraper import MarvelDatabaseScraper
import re


class MarvelDatabaseIssue(Page):

    def __init__(self, **kwargs):
        super(MarvelDatabaseIssue, self).__init__(**kwargs)
        scraper = MarvelDatabaseScraper()
        if 'title' in kwargs:
            self.title = kwargs['title']
        else:
            self.title = None
        if 'volume' in kwargs:
            self.volume = kwargs['volume']
        else:
            self.volume = None
        if 'issue_number' in kwargs:
            self.issue_number = kwargs['issue_number']
        else:
            self.issue_number = None
        if not self.page and self.title is not None and self.volume is not None and self.issue_number is not None:
            self.page = scraper.fetch_page(f'{self.title}_Vol_{self.volume}_{self.issue_number}')

        self.publish_date = scraper.get_published_date(self.page)
        self.release_date = scraper.get_release_date(
            self.page,
            title=self.title,
            issue_number=self.issue_number,
            year=self.year_published
        )

    @property
    def info_table(self):
        info_box = self.page.html.find('.infobox')
        return info_box[0].find('table')

    @property
    def writer(self):
        return self.get_detail('Writer')

    @property
    def penciler(self):
        return self.get_detail('Penciler')

    @property
    def inker(self):
        return self.get_detail('Inker')

    @property
    def colourist(self):
        return self.get_detail('Colourist')

    @property
    def letterer(self):
        return self.get_detail('Letterer')

    @property
    def editor(self):
        return self.get_detail('Editor')

    @property
    def cover_artists(self):
        return self.get_detail('Cover Artist')

    @property
    def year_published(self):
        return re.split(r'-', self.publish_date)[-1]

    def _get_info_table_divs(self):
        return self.info_table[0].find('div')

    def get_detail(self, regex: str):
        detail = ''
        for div in self._get_info_table_divs():
            if re.search(f'^{regex}\n.+\n', div.text):
                for text in re.split(r'\n', div.text):
                    if not re.search(f'{regex}', text):
                        if detail == '':
                            detail = text
                        else:
                            detail += f' and {text}'
                break
            elif re.search(f'^{regex}\n', div.text):
                detail = re.split(r'\n', div.text)[-1]
                break
        return 'n/a' if detail == '' else detail

    def get_main_cover(self, title, image_extension='png'):
        cover_area = self.page.html.find('.floatnone')
        if len(cover_area) > 1:
            cover_element = cover_area[1].find('a')
        else:
            cover_element = cover_area[0].find('a')
        cover_links = cover_element[0].links
        cover_link = self._get_cover_link(cover_links)
        artists = self.cover_artists
        scraper = MarvelDatabaseScraper()
        scraper.download_image(
            cover_link,
            f'{title} by {artists}.{image_extension}',
            directory=f'covers/{self.title}/Vol {self.volume}'
        )

    def get_variant_covers(self, title, image_extension):
        divs = self._get_info_table_divs()[0].find('div')
        for div in divs:
            if re.search(r'(Variant|Textless)', div.text):
                if re.search(r'^Textless$', div.text):
                    save_title = f'{title} {div.text} Variant by {self.cover_artists}'
                else:
                    save_title = f'{title} {div.text}'
                cover_link = self._get_cover_link(div.links)
                scraper = MarvelDatabaseScraper()
                scraper.download_image(
                    cover_link,
                    f'{save_title}.{image_extension}',
                    directory=f'covers/{self.title}/variants'
                )

    @staticmethod
    def _get_cover_link(cover_links):
        cover_link = ''
        for link in cover_links:
            if re.search(r'/images/', link):
                cover_link = link
                break
        return cover_link

    def dump_to_dict(self):
        return {
            'title': self.title,
            'volume': self.volume,
            'issue number': self.issue_number,
            'writer': self.writer,
            'editor': self.editor,
            'inker': self.inker,
            'colourist': self.colourist,
            'pensiler': self.pensiler,
            'letterer': self.letterer
        }

