from requests_html import HTMLResponse


class Page:

    def __init__(self, **kwargs):
        if 'page' in kwargs:
            self._page = kwargs['page']
        else:
            self._page = None

    @property
    def page(self):
        return self._page

    @page.setter
    def page(self, page: HTMLResponse):
        self._page = page