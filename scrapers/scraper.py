from requests_html import HTMLSession, HTMLResponse
from urllib import request
import re
from http import HTTPStatus
from datetime import datetime
from pathlib import Path


def sanitize_file_name(name):
    name = re.sub(r':', ';', name)
    name = re.sub(r'\n', ' ', name)
    name = re.sub(r'<', '[', name)
    name = re.sub(r'>', ']', name)
    name = re.sub(r' ', '.', name)
    name = re.sub(r'\?|\||\/|\\|"', '', name)
    return name


def sanitize_dir_name(name):
    name = re.sub(r':', ';', name)
    name = re.sub(r'\n', ' ', name)
    name = re.sub(r'<', '[', name)
    name = re.sub(r'>', ']', name)
    name = re.sub(r' ', '.', name)
    return name


class Scraper:

    def __init__(self):
        self.session = HTMLSession()

    @staticmethod
    def _add_signature(url, signature='https'):
        if not re.search(r'(http://)|(https://)', url):
            if not re.search(r'(http:)|(https:)', url) and re.search(r'^//', url):
                url = f'{signature}:{url.lstrip()}'
            elif re.search(r'^://', url) and not re.search(r'(http|https)', url):
                url = f'{signature}{url.lstrip()}'
            else:
                url = f'{signature}://{url.lstrip()}'
        elif not re.search(f'{signature}:', url):
            if re.search(r'http:', url):
                url = re.sub('http', signature, url)
            else:
                url = re.sub('https', signature, url)
        return url.strip()

    @staticmethod
    def _sanitize_url(url):
        url = re.sub(r"'", '%27', url)
        return re.sub(r' ', '_', url).strip()

    def _request(self, base_url, extension: str = '') -> HTMLResponse:
        return self.session.get(
            self._add_signature(
                self._sanitize_url(f'{base_url}/{extension}')
            )
        )

    @staticmethod
    def download_image(link, name, directory='outputs'):
        directory = sanitize_dir_name(directory)
        Path(directory).mkdir(exist_ok=True, parents=True)
        request.urlretrieve(link, f'{directory}/{sanitize_file_name(name)}')

    @staticmethod
    def convert_absolute_links_to_list(links):
        absolute_links = []
        if type(links) is set:
            for absolute_link in links:
                absolute_links.append(absolute_link)
        else:
            for absolute_link in links.absolute_links:
                absolute_links.append(absolute_link)
        return absolute_links

    @staticmethod
    def is_response_ok(response):
        return response.status_code == HTTPStatus.OK.value

    @staticmethod
    def format_dates(d, out_format, in_format=None) -> str:
        try:
            if type(d) is str:
                d = datetime.strptime(d, in_format)
                d = d.strftime(out_format)
            elif type(d) is datetime:
                d = d.strftime(out_format)
            else:
                raise TypeError(f'Expected str or datetime got {type(d)} instead.')
        except ValueError:
            d = 'n/a'
        return d

    @staticmethod
    def get_absolute_links(link):
        absolute_links = []
        for absolute_link in link.absolute_links:
            absolute_links.append(absolute_link)
        return absolute_links
