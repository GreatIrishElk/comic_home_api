from scrapers.scraper import Scraper
from scrapers.marvel_website_scraper import MarvelWebsiteScraper
from pathlib import Path
from http import HTTPStatus
import os
import re
from requests_html import HTMLResponse
import requests


def get_root_dir():
    current_dir = os.path.dirname(os.path.realpath(__file__))
    root_dir = None
    for p in Path(current_dir).parent.parts:
        if root_dir:
            root_dir += f"/{p}"
        else:
            root_dir = p
    return root_dir


MARVEL_DATABASE = "https://marvel.fandom.com/"


class MarvelDatabaseScraper(Scraper):

    def _request(self, base_url=MARVEL_DATABASE, extension: str = '') -> HTMLResponse:
        return super(MarvelDatabaseScraper, self)._request(base_url=base_url, extension=extension)

    @staticmethod
    def _get_cover_link(cover_links):
        cover_link = ''
        for link in cover_links:
            if re.search(r'/images/', link):
                cover_link = link
                break
        return cover_link

    def fetch_issue(self, title, volume, issue) -> HTMLResponse:
        return self.fetch_page(f'{title}_Vol_{volume}_{issue}')

    def get_issue(self, title) -> HTMLResponse:
        return self._request(extension=title)

    def fetch_page(self, extension) -> HTMLResponse:
        return self._request(extension=self._sanitize_url(f'wiki/{extension}'))

    def is_valid_title(self, title, vol='1'):
        response = self._request(
            extension=self._sanitize_url(f'{title} Vol {vol}')
        )
        return response.status_code == HTTPStatus.OK

    @staticmethod
    def get_info_table_tables(response: HTMLResponse):
        info_box = response.html.find('.infobox')
        return info_box[0].find('table')

    @staticmethod
    def get_info_table_divs(response: HTMLResponse):
        info_box = response.html.find('.infobox')
        return info_box[0].find('div')

    def get_issues(self, title, vol=None, response=None):
        if vol is not None and response is None:
            response = self.fetch_page(f'{title}_Vol_{str(vol)}')
        issues = response.html.find('.wikia-gallery-item')
        issue_addresses = []
        for issue in issues:
            for link in issue.links:
                if re.search(f'^/wiki/{self._sanitize_url(title)}_Vol_\d+_\d', link):
                    issue_link = link
                    issue_addresses.append(self._sanitize_url(issue_link))
                    break
        return issue_addresses

    def get_main_cover(self, response: HTMLResponse, title, image_extension, directory='outputs'):
        cover_area = response.html.find('.floatnone')
        if len(cover_area) > 1:
            cover_element = cover_area[1].find('a')
        else:
            cover_element = cover_area[0].find('a')
        cover_links = cover_element[0].links
        cover_link = self._get_cover_link(cover_links)
        artists = self.get_cover_artists(response)
        self.download_image(cover_link, f'{title} by {artists}.{image_extension}', directory=directory)

    def get_variant_title(self, response, div_text):
        if re.search(r'^Textless$', div_text):
            save_title = f'{div_text} Variant by {self.get_cover_artists(response)}'
        else:
            save_title = f'{div_text}'
        return save_title

    def get_variant_covers(self, response: HTMLResponse, title, image_extension, directory='outputs'):
        divs = self.get_info_table_tables(response)[0].find('div')
        for div in divs:
            variant_title = self.get_variant_title(response, div.text)
            if re.search(r'(Variant|Textless)', div.text):
                save_title = f'{title} {variant_title}'
                cover_link = self._get_cover_link(div.links)
                self.download_image(cover_link, f'{save_title}.{image_extension}', directory=directory)

    def get_release_date(self, response: requests.Response, title, year, issue_number):
        release_date = None
        for div in self.get_info_table_tables(response):
            if re.search(r'^Published\nReleased', div.text):
                release_date = re.split(r'\n', div.text)[3]
                break
        if not release_date:
            s = MarvelWebsiteScraper()
            release_date = s.get_release_date(title, year, issue_number)
        else:
            release_date = self.format_dates(release_date, in_format='%B %d, %Y', out_format='%d-%m-%Y')
        return release_date

    def get_published_date(self, response: HTMLResponse):
        published_date = None
        for div in self.get_info_table_tables(response):
            if re.search(r'^Published\n\w+, \d\d\d\d$', div.text):
                published_date = re.split(r'\n', div.text)[1]
                break
            elif re.search(r'^Published\nReleased', div.text):
                published_date = re.split(r'\n', div.text)[2]
                break
        if not published_date:
            published_date = 'n/a'
        else:
            published_date = self.format_dates(published_date, in_format='%B, %Y', out_format='%m-%Y')
        return published_date

    def get_writer(self, response: HTMLResponse):
        return self.get_detail(response, 'Writer')

    def get_penciler(self, response: HTMLResponse):
        return self.get_detail(response, 'Penciler')

    def get_inker(self, response: HTMLResponse):
        return self.get_detail(response, 'Inker')

    def get_colourist(self, response: HTMLResponse):
        return self.get_detail(response, 'Colourist')

    def get_letterer(self, response: HTMLResponse):
        return self.get_detail(response, 'Letterer')

    def get_editor(self, response: HTMLResponse):
        return self.get_detail(response, 'Editor')

    def get_cover_artists(self, response: HTMLResponse):
        return self.get_detail(response, 'Cover Artist')

    def get_detail(self, response: HTMLResponse, regex: str):
        detail = ''
        for div in self.get_info_table_divs(response):
            if re.search(f'^{regex}\n.+\n', div.text):
                for text in re.split(r'\n', div.text):
                    if not re.search(f'{regex}', text):
                        if detail == '':
                            detail = text
                        else:
                            detail += f' and {text}'
                break
            elif re.search(f'^{regex}\n', div.text):
                detail = re.split(r'\n', div.text)[-1]
                break
        return 'n/a' if detail == '' else detail

