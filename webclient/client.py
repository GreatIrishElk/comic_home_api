#!/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler
from http import HTTPStatus
from threading import Thread
from socketserver import ThreadingMixIn
from webclient.comics_api import ComicsAPI, APICommands
import json
import socket

PORT = 8080


class Handler(BaseHTTPRequestHandler):
    
    def do_GET(self):
        comics_api = ComicsAPI()
        command, options = comics_api.get_options(self.path)
        print(command)
        print(options)
        if command == APICommands.GET_DATE.value:
            date = comics_api.get_date(options['title'], options['vol'], options['issue'])
            date = json.dumps(date)
            self.send_response(HTTPStatus.OK.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes(date, "utf-8"))
        elif command == APICommands.GET_COMIC.value:
            details = comics_api.get_details(options['title'], options['vol'], options['issue'])
            details = json.dumps(details)
            self.send_response(HTTPStatus.OK.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes(details, "utf-8"))
        elif command == APICommands.SET_BACKGROUND.value:
            self.send_response(HTTPStatus.NOT_IMPLEMENTED.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
        elif command == APICommands.REQUEST_COVER.value:
            comics_api.request_cover(options['title'], options['vol'], options['issue'])
            self.send_response(HTTPStatus.OK.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
        elif command == APICommands.REQUEST_COVER_VARIANT.value:
            comics_api.request_variant_cover(options['title'], options['vol'], options['issue'])
            self.send_response(HTTPStatus.OK.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
        elif command == APICommands.NEXT_BACKGROUND.value:
            comics_api.write_command(APICommands.NEXT_BACKGROUND.value)
            self.send_response(HTTPStatus.OK.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
        elif command == APICommands.STOP.value:
            comics_api.write_command(APICommands.STOP.value)
            self.send_response(HTTPStatus.OK.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
        else:
            self.send_response(HTTPStatus.NOT_FOUND.value)
            self.send_header("Content-type", "text/plain")
            self.end_headers()


class ThreadingHTTPServer(ThreadingMixIn, HTTPServer):
    daemon_threads = True


def serve_on_port(port):
    host_name = socket.gethostname()
    ip = socket.gethostbyname(host_name)
    print(f'Running server on {ip}')
    server = ThreadingHTTPServer((ip, port), Handler)
    server.serve_forever()


if __name__ == '__main__':
    Thread(target=serve_on_port, args=[PORT]).start()

