import pytest
from scrapers.scraper import Scraper
from rich import print


class Links:
    def __init__(self):
        self.absolute_links = {1, 2, 3}


ABS_LINKS = Links()


@pytest.mark.unittests
class TestScraper:

    @pytest.mark.parametrize(
        'url, signature, expected_url', [
            ('www.sample', 'http', 'http://www.sample'),
            ('//www.sample', 'http', 'http://www.sample'),
            ('://www.sample', 'http', 'http://www.sample'),
            ('https://www.sample', 'http', 'http://www.sample'),
            ('www.sample', 'https', 'https://www.sample'),
            ('//www.sample', 'https', 'https://www.sample'),
            ('://www.sample', 'https', 'https://www.sample'),
            ('http://www.sample', 'https', 'https://www.sample'),
        ]
    )
    def test_add_signature(self, url, signature, expected_url):
        scraper = Scraper()
        actual_url = scraper._add_signature(
            url=url,
            signature=signature
        )
        assert actual_url == expected_url, \
            f'[bold green] For URL:[/bold green] [red]{url}[/red]' \
            f'[bold green]Expected:[/bold red] [red]{expected_url}[/red]' \
            f'[bold green]Actual:[/bold red] [red]{actual_url}[/red]'

    @pytest.mark.parametrize(
        'ab_links', [
            {1, 2, 3},
            ABS_LINKS
        ]
    )
    def test_convert_absolute_links_to_list(self, ab_links):
        scraper = Scraper()
        link = scraper.convert_absolute_links_to_list(ab_links)
        assert type(link) is list, \
            f'[bold green] For set:[/bold green] [red]{ab_links}[/red]' \
            f'[bold green]Expected:[/bold red] [red]list[/red]' \
            f'[bold green]Actual:[/bold red] [red]{type(link)}[/red]'

    @pytest.mark.parametrize(
        'date, in_format, out_format, expected_date', [
            ('10-09-2020', '%d-%m-%Y', '%d/%m/%Y', '10/09/2020'),
            ('05-10-2020', '%m-%d-%Y', '%d/%m/%Y', '10/05/2020'),
            ('10-2020', '%m-%Y', '%m/%Y', '10/2020'),
            ('2020', '%m-%Y', '%m/%Y', 'n/a')
        ]
    )
    def test_format_date(self, date, in_format, out_format, expected_date):
        scraper = Scraper()
        actual_date = scraper.format_dates(
            d=date,
            in_format=in_format,
            out_format=out_format
        )
        assert actual_date == expected_date, \
            f'[bold green] For date:[/bold green] [red]{date}[/red]' \
            f'[bold green]Expected:[/bold red] [red]{expected_date}[/red]' \
            f'[bold green]Actual:[/bold red] [red]{actual_date}[/red]'

    def test_format_date_raise_type_error(self):
        scraper = Scraper()
        with pytest.raises(TypeError, match=f"Expected str or datetime got <class 'int'> instead."):
            scraper.format_dates(1, in_format='', out_format='')

